-- latihan 1
-- soal latihan bab 9-10 buku the craft of functional programming

-- buat fungsi length dengan map dan sum
length' xs = sum ( map (\x -> x/x) xs ) 

-- fungsi ini menambahkan 1 utk setiap elemen xs, 
-- dan menambahkan 1 utk setiap elemen list hasil fungsi map bagian dalam
tambah2 xs = map (+1) ( map (+1) xs )

-- sum of square 1 - n , using map and foldr
-- sum2 n = foldr ( + ) 0 ( map (/x -> x * x ) [1..n] ) tanda /x nya kebalik harusnya \x
sum2 n = foldr ( + ) 0 ( map (\x -> x * x ) [1..n] )


-- hasil map adalah [ [], [], [] ]
-- hasil foldr, yang tadinya list of list, jadi list of angka
-- jadi balik lagi ke bentuk awal
misteri1 xs = (map sing xs)
            where
                sing x = [x]
misteri2 xs = foldr (++) [] (misteri1 xs) -- biar aman, pakai kurung terus
misteri xs = foldr (++) [] (map sing xs)
                where 
                    sing x = [x]

-- input flip itu fungsi dengan 2 parameternya
flip2 :: (a -> b -> c) -> (b -> a -> c)
flip2 f a b = f b a


-- ubah list comprehension jadi higher order functions pakai map, filter, dan concat
--- [x+1 | x <- xs]
l1 xs = map (\x -> x + 1) xs

-- [ x+y | x <- xs, y <- ys ]
l2 xs ys = concat ( map (\x -> map (\y -> x+y ) ys) xs )

-- [ x+2 | x <- xs, x > 3]
l3 xs = map (\x -> x + 2) ( filter (>3) xs )

-- [ x+3 | (x, _) <- xys ]
l4 xys = map (\a -> ( fst a ) + 3) xys

-- [ x+5 | Just x <- mxs ]
-- l5 xms = map (\x -> (Just x) + 5 ) xms
-- masih gak tau


-- sebaliknya, fungsi jadiin list comprehension
-- map (+3) xs
l1' xs = [x+3 | x <- xs]

-- filter (>7) xs
l2' xs = [x | x <- xs, x > 7]


--concat (map (\x -> map (\y -> (x,y)) ys ) xs)
l3' xs ys = [(x,y) | x <- xs, y <- ys]

-- filter (>3) (map (\(x,y) -> x+y) xys) --hasil penjumlahan baru di filter
-- l4' xys = [x+y | (x,y) <- xys, x > 3]  -- ini difilter dulu, baru di jumlahin
l4' xys = [x+y | (x,y) <- xys, x+y > 3] 



-- latihan 2
-- list comprehension dan lazy evaluation

-- soal 1
-- pertama ambil nilai x = 1
-- iterasi semua nilai yang ada di y
-- cek x > y
-- jika iya lakukan x + y
-- ambil nilai x selanjutnya
-- iterasi lagi semua nilai y
[x+y | x <- [1..4], y <- [2..4], x > y]


-- soal 2
divisor n = [x | x <- [1..n], 12 `mod` x == 0]


-- soal 3
-- definisi quicksort dengan list comprehension
quicksort [] = []
quicksort (x:xs) = quicksort[a | a <- xs, a <= x] ++ [x] ++ quicksort[b | b <- xs, b > x]

-- soal 4
-- definisi infinite list untuk permutation
-- import Data.List -- jangan lupa import dulu
-- [1,2,3] \\ [1]  -- ini utk cari beda list
-- > [2,3]
p [] = [[]]
p xs = [x:sisa | x <- xs, sisa <- p (xs \\ [x])


-- soal 5
-- definisi untuk memberikan infinite list dari bilangan prima dgn algoritma sieve of erasthotenes
prima = sieve[2..]
        where
            sieve (x:xs) = x : sieve[y | y <- xs, y `mod` x /= 0]


-- soal 6
-- triple pythagoras
pt = [(x,y,z) | z <- [5..], y <- [z,z-1..1], x <- [y,y-1..1], x*x + y*y == z*z]


-- kuis
-- soal 1
max3 a b c = if ( max a b ) >= c then max a b else c

-- soal 4
jumlahList xs = foldr (+) 0 xs

-- soal 7
flip3 :: (a -> b -> c) -> (b -> a -> c)
flip3 f a b = f b a



-- fungsi update state
updateState :: (RobotState -> RobotState) -> Robot ()
updateState u = Robot (\s _ _ -> return (u s, () ) )



---
-- fungsi mapM_
mapM_ :: Monad a => (b -> a c) -> [b] -> a ()
-- mapM_ typeClass nya adalah Monad.
-- variabel a, tipenya harus merupakan anggota dari Monad.
-- mapM_ menerima 2 input. fungsi dan list.
-- mapM_ memetakan fungsi ke tiap anggota list. tapi hasil fungsi itu tidak disimpan. mapM_ hanya perlu efek nya saja.
-- beda dengan mapM yang menyimpan hasil fungsi.

-- penjelasan dari jawaban stackoverflow | https://stackoverflow.com/questions/27609062/what-is-the-difference-between-mapm-and-mapm-in-haskell/27609146

{-
The core idea is that mapM maps an "action" (ie function of type a -> m b) over a list and gives you all the results as a m [b]. mapM_ does the same thing, but never collects the results, returning a m ().

If you care about the results of your a -> m b function (ie the bs), use mapM. If you only care about the effect, whatever it is, but not the resulting value, use mapM_ because it can be more efficient and, more importantly, makes your intentions clear.

You would always use mapM_ with functions of the type a -> m (), like print or putStrLn. These functions return () to signify that only the effect matters. If you used mapM, you'd get a list of () (ie [(), (), ()]), which would be completely useless but waste some memory. If you use mapM_, you would just get a (), but it would still print everything.

On the other hand, if you do care about the returned values, use mapM. As a hypothetical example, imagine a function fetchUrl :: Url -> IO Response—chances are, you care about the response you get for each URL. So for this, you'd use mapM to get a lists of responses out that you can then use in the rest of your code.

So: mapM if you care about the results and mapM_ if you don't.

Normal map is something different: it takes a normal function (a -> b) instead of one using a monad (a -> m b). This means that it cannot have any sort of effect besides returning the changed list. You would use it if you want to transform a list using a normal function. map_ doesn't exist because, since you don't have any effects, you always care about the results of using map.
-}

-- ex:
moven n = mapM_ (const move) [1..n]